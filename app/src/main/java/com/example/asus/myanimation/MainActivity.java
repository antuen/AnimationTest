package com.example.asus.myanimation;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout mLinearLayout;
    private ImageView mImageView;
    private Button mStartButton;
    private Button mStopButton;
    private Button mTranslateButton;
    private Button mScaleButton;
    private Button mRotateButton;
    private Button mAlphaButton;
    private Button mAnimationButton;
    private AnimationDrawable animationDrawable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLinearLayout=findViewById(R.id.layout);
        mImageView=(ImageView) findViewById(R.id.frame_attack);
        mStartButton=findViewById(R.id.bt_start);
        mStopButton=findViewById(R.id.bt_stop);
        mTranslateButton=findViewById(R.id.btn_translate);
        mScaleButton=findViewById(R.id.btn_scale);
        mRotateButton=findViewById(R.id.btn_rotate);
        mAlphaButton=findViewById(R.id.btn_alpha);
        mAnimationButton=findViewById(R.id.btn_animation);
        mTranslateButton.setOnClickListener(this);
        mScaleButton.setOnClickListener(this);
        mRotateButton.setOnClickListener(this);
        mAlphaButton.setOnClickListener(this);
        mAnimationButton.setOnClickListener(this);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageView.setImageResource(R.drawable.attack);
                animationDrawable=(AnimationDrawable)mImageView.getDrawable();
                animationDrawable.start();
            }
        });
        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageView.setImageResource(R.drawable.attack);
                animationDrawable=(AnimationDrawable)mImageView.getDrawable();
                animationDrawable.stop();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_translate:
                mLinearLayout.invalidate();
                Animation translateAnimation= new TranslateAnimation(0,500,0,500);
                translateAnimation.setDuration(3000);
                translateAnimation.setFillBefore(false);
                translateAnimation.setFillAfter(true);
                translateAnimation.setRepeatMode(2);
                translateAnimation.setRepeatCount(1);
                mTranslateButton.setAnimation(translateAnimation);
                break;
            case R.id.btn_scale:
                mLinearLayout.invalidate();
                Animation scaleAnimation= new ScaleAnimation(1,2,1,2,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
                scaleAnimation.setDuration(3000);
                scaleAnimation.setFillBefore(false);
                scaleAnimation.setFillAfter(true);
                scaleAnimation.setRepeatMode(2);
                scaleAnimation.setRepeatCount(1);
                mScaleButton.setAnimation(scaleAnimation);
                break;
            //onWindowFocusChanged(true);
            case R.id.btn_rotate:
                mLinearLayout.invalidate();
                Animation rotateAnimation= new RotateAnimation(0,360,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
                rotateAnimation.setDuration(3000);
                rotateAnimation.setFillBefore(false);
                rotateAnimation.setFillAfter(true);
                rotateAnimation.setRepeatMode(2);
                rotateAnimation.setRepeatCount(1);
                mRotateButton.setAnimation(rotateAnimation);
                break;
            case R.id.btn_alpha:
                mLinearLayout.invalidate();
                Animation alphaAnimation= new AlphaAnimation(1,0);
                alphaAnimation.setDuration(3000);
                alphaAnimation.setFillBefore(false);
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setRepeatMode(2);
                alphaAnimation.setRepeatCount(1);
                mAlphaButton.setAnimation(alphaAnimation);
                break;
            case R.id.btn_animation:
                startActivity(new Intent(MainActivity.this,AnimationActity.class));
                finish();
        }
    }
}
