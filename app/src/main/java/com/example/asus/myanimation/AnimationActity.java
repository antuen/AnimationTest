package com.example.asus.myanimation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by a'su's on 2018/7/17.
 */

public class AnimationActity extends AppCompatActivity {
    private ImageView mSunIV;
    private ImageView mBackgroundIV;
    private ImageView mCloud1IV;
    private ImageView mCloud2IV;
    private ImageView mSpringIv;
    private Button mSpringBt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        init();
    }

    private void init() {
        mBackgroundIV=findViewById(R.id.iv_animation);
        mCloud1IV=findViewById(R.id.iv_cloud1);
        mCloud2IV=findViewById(R.id.iv_cloud2);
        mSunIV=findViewById(R.id.iv_sun);
        mSpringIv=findViewById(R.id.iv_spring_animation);
        mSpringBt=findViewById(R.id.btn_spring_animation);

        SpringForce springForce=new SpringForce(0)
                .setDampingRatio(0)
                .setStiffness(0.5f);
        final SpringAnimation animation=new SpringAnimation(mSpringIv,SpringAnimation.ROTATION_X)
                .setSpring(springForce);
        mSpringBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation.cancel();
                //animation.setStartValue(-200);
                animation.setStartVelocity(500);
                animation.start();
            }
        });

        AnimatorSet animatorSet=new AnimatorSet();

        ObjectAnimator objectAnimator1=(ObjectAnimator)ObjectAnimator.ofFloat(mSunIV,"TranslationY",0,-200);
        objectAnimator1.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator1.setRepeatCount(1);
        objectAnimator1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationRepeat(Animator animation) {
                super.onAnimationRepeat(animation);
                mSunIV.setImageResource(R.mipmap.moon);
            }

        });

        ObjectAnimator objectAnimator2=(ObjectAnimator)ObjectAnimator.ofFloat(mSunIV,"rotation",0,360);
        objectAnimator2.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator2.setRepeatCount(1);


        ObjectAnimator objectAnimator3=(ObjectAnimator)ObjectAnimator.ofFloat(mSunIV,"alpha",1, 0);
        objectAnimator3.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator3.setRepeatCount(1);
        objectAnimator3.setDuration(2000);

        ObjectAnimator objectAnimator4=(ObjectAnimator)ObjectAnimator.ofFloat(mCloud1IV,"TranslationX",0,-200);
        objectAnimator4.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator4.setRepeatCount(1);

        ObjectAnimator objectAnimator5=(ObjectAnimator)ObjectAnimator.ofFloat(mCloud1IV,"alpha",1, 0);
        objectAnimator5.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator5.setRepeatCount(1);
        objectAnimator5.setDuration(2000);

        ObjectAnimator objectAnimator6=(ObjectAnimator)ObjectAnimator.ofFloat(mCloud2IV,"TranslationX",0,200);
        objectAnimator6.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator6.setRepeatCount(1);

        ObjectAnimator objectAnimator7=(ObjectAnimator)ObjectAnimator.ofFloat(mCloud2IV,"alpha",1, 0);
        objectAnimator7.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator7.setRepeatCount(1);
        objectAnimator7.setDuration(2000);

        ObjectAnimator objectAnimator8=(ObjectAnimator)ObjectAnimator.ofFloat(mBackgroundIV,"alpha",1, (float)0.2);
        objectAnimator8.setRepeatMode(ObjectAnimator.REVERSE);
        objectAnimator8.setRepeatCount(1);
        objectAnimator8.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationRepeat(Animator animation) {
                super.onAnimationRepeat(animation);
                mBackgroundIV.setBackgroundColor(Color.BLUE);
            }
        });
        objectAnimator8.setDuration(2000);

        animatorSet.playTogether(objectAnimator1,objectAnimator2,objectAnimator3,objectAnimator4,objectAnimator5,objectAnimator6,objectAnimator7,objectAnimator8);
        animatorSet.setDuration(2000);
        animatorSet.start();
    }
}
